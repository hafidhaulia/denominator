This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## You can see it online from this link:
[Denominator by Hafidh Aulia Wirandi](https://denominator-4ccd7.firebaseapp.com)

## You can clone it from this repository
[Repository Denominator by Hafidh Aulia Wirandi](https://hafidhaulia@bitbucket.org/hafidhaulia/denominator.git)

### Available scripts

#### `yarn install`

Run this script after clone it from the repository

#### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

#### `yarn test`

Launches the test runner in the interactive watch mode.<br>