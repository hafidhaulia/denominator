import React from 'react';
import './App.css';
import { validator } from './utils/validator';
import { parser } from './utils/parser';
import { denominator, moneySheet } from './utils/denominator';

interface State {
  input: string;
  error?: Error;
  change: number[];
  showNominal: boolean;
}

interface Props {}

class App extends React.Component<Props,State> {

  constructor(props: Props){
    super(props);
    this.state = {
      input: '',
      error: undefined,
      change: [],
      showNominal: false
    }
  }

  onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ 
      input: e.target.value,
      error: undefined
     });
  }

  onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    const { input } = this.state;
    const validatedInput = validator(input);

    this.setState({ change: []});

    if(validatedInput instanceof Error) {
      this.setState({ error: validatedInput, showNominal: false });
    }
    else {
      const num = parser(validatedInput);
      const change = denominator(num);
      this.setState({ change, showNominal: true });
    }
  }

  render() {
    const { input, error, change, showNominal } = this.state;

    return (
      <React.Fragment>
        <div className="container">
          <div className="column"></div>
          <div className="form-wrapper">
            <form onSubmit={this.onSubmit} className="form">
              <input type="text" onChange={this.onChange} value={input}/>
              <p className="helper-text">Tekan 'enter' untuk melakukan perhitungan</p>
              <p className="error-text">{error && error.message}</p>
            </form>
          </div>
          <div className="column"></div>
        </div>
        {showNominal && 
          <div className="change">
            <h3>Pecahan uang</h3>
            <ul>
            {
              change.map((sheet, index) => {
                if(sheet) {
                  if (moneySheet[index]!=null) {
                    return <li key={index}>{sheet} lembar <strong>Rp{moneySheet[index]}</strong></li>;
                  }
                  return (
                    <li key={index}>tersisa <strong>Rp{sheet}</strong></li>
                  )
                }
                return null;
              })
            }
            </ul>
          </div>
        }
        <p className="developer-text">Develop by Hafidh Aulia Wirandi</p>
      </React.Fragment>
    );
  }
}

export default App;
