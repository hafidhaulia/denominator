export const parser = (text: string): number => {
  const arr: string[] | null = text.match(/\d/g);
  return arr ? parseInt(arr.join("")) : 0;
}
