export const moneySheet: number[] = [
  100000,
  50000,
  20000,
  10000,
  5000,
  1000,
  500,
  100,
  50
];

export const denominator = (money: number): number[] => {
  let result: number[] = [];
  moneySheet.forEach(sheet => {
    if(money === 0) {
      result = [...result, 0];
      return;
    }
    let counter = 0;
    while (money >= sheet) {
      money -= sheet;
      counter++;
    }
    result = [...result, counter];
  });
  return (money !== 0) ? [...result, money] : result;
};
