import {  parser } from './parser';

it('18.650', () => {
  expect(parser('18.650')).toBe(18650);
});

it('018.650', () => {
  expect(parser('018.650')).toBe(18650);
});

it('018650', () => {
  expect(parser('018650')).toBe(18650);
});