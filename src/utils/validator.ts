export const regexContainSpace: RegExp = /\s/;
export const regexContainCurrency: RegExp = /rp/;
export const regexContainNumber: RegExp = /\d/;
export const regexValidPositionCurrency: RegExp = /^rp/;
export const regexContainPrefix: RegExp = /,\d\d$/;
export const regexValidMoney: RegExp = /^(?!0\.00)\d{1,3}(((\.)?\d{3})*)?$/;

export const validator = (input: string): string | Error => {
  let text = input.toLowerCase();
  if (regexContainSpace.test(text)) {
    return new Error("Invalid separator");
  }

  if (regexContainCurrency.test(text)) {
    if(!regexContainNumber.test(text)) {
      return new Error("Missing value");
    }
    if(!regexValidPositionCurrency.test(text)) {
      return new Error("Valid character in wrong position");
    }
    text = removeCurrency(text);
  }

  if(regexContainPrefix.test(text)) {
    text = removePrefix(text);
  }

  if(!regexContainNumber.test(text)) {
    return new Error("Invalid format");
  }

  if(!regexValidMoney.test(text)) {
    return new Error("Invalid separator");
  }

  return text;
}

export const removeCurrency = (text: string): string => {
  return text.slice(2);
}

export const removePrefix = (text: string): string => {
  return text.slice(0,text.length-3);
}