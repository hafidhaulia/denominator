import { denominator} from './denominator';

describe('test simple denominator', () => {
  it('120000', () => {
    const sheet = denominator(120000);
    expect(sheet).toStrictEqual([1,0,1,0,0,0,0,0,0]);
  });

  it('2000000', () => {
    const sheet = denominator(2000000);
    expect(sheet).toStrictEqual([20,0,0,0,0,0,0,0,0]);
  });

  it('186650', () => {
    const sheet = denominator(186650);
    expect(sheet).toStrictEqual([1,1,1,1,1,1,1,1,1]);
  });

  it('186750', () => {
    const sheet = denominator(186750);
    expect(sheet).toStrictEqual([1,1,1,1,1,1,1,2,1]);
  });

});