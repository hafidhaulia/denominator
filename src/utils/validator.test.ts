import { 
  regexContainCurrency,
  regexValidPositionCurrency,
  regexValidMoney,
  removeCurrency,
  removePrefix,
  validator } from './validator';

describe('contain prefix', () => {
  it('no currency', () => {
    expect(regexContainCurrency.test('2000')).toBe(false);
  })
  it('currency in the start', () => {
    expect(regexContainCurrency.test('rp2000')).toBe(true);
  });
  it('currency in the middle', () => {
    expect(regexContainCurrency.test('20rp00')).toBe(true);
  });
  it('currency in the end', () => {
    expect(regexContainCurrency.test('2000rp')).toBe(true);
  });
});

describe('check currency format', () => {
  it('valid format', () => {
    expect(regexValidPositionCurrency.test('rp2000')).toBe(true);
  });
  it('wrong position', () => {
    expect(regexValidPositionCurrency.test('2000rp')).toBe(false);
  });
});

describe('check rupiah format', () => {
  it('2650', () => {
    expect(regexValidMoney.test('2650')).toBe(true);
  });
  it('2.650', () => {
    expect(regexValidMoney.test('2.650')).toBe(true);
  });
  it('26.50', () => {
    expect(regexValidMoney.test('26.50')).toBe(false);
  });
  it(',2650', () => {
    expect(regexValidMoney.test(',2650')).toBe(false);
  });
  it(',2.650', () => {
    expect(regexValidMoney.test(',2.650')).toBe(false);
  });
  it('002650', () => {
    expect(regexValidMoney.test('002650')).toBe(true);
  });
  it('002.650', () => {
    expect(regexValidMoney.test('002.650')).toBe(true);
  });
  it('002,650', () => {
    expect(regexValidMoney.test('002,650')).toBe(false);
  });
  it('2,650', () => {
    expect(regexValidMoney.test('2,650')).toBe(false);
  });
  it('.2650', () => {
    expect(regexValidMoney.test('.2650')).toBe(false);
  });
});

describe('remove currency',() => {
  it('remove rp', () => {
    expect(removeCurrency('rp2650')).toEqual('2650');
  });
  it('remove rp.', () => {
    expect(removeCurrency('rp.2650')).toEqual('.2650');
  });
  it('remove rp only', () => {
    expect(removeCurrency('rp,2650')).toEqual(',2650');
  });
});

describe('remove prefix',() => {
  it('remove ,00', () => {
    expect(removePrefix('2650,00')).toEqual('2650');
  });
  it('remove ,12', () => {
    expect(removePrefix('2650,12')).toEqual('2650');
  });
});

describe('test validator', () => {
  const invalidError = new Error("Invalid format");
  const invalidSeparator = new Error("Invalid separator");
  const wrongPosError = new Error("Valid character in wrong position");
  const missingError = new Error("Missing value");

  it('18015', () => {
    expect(validator('18015')).toBe('18015');
  });

  it('18.015', () => {
    expect(validator('18.015')).toBe('18.015');

  });

  it('Rp18015', () => {
    expect(validator('Rp18015')).toBe('18015');
  });

  it('Rp18.015', () => {
    expect(validator('Rp18.015')).toBe('18.015');
  });

  it('Rp18015,00', () => {
    expect(validator('Rp18015,00')).toBe('18015');
  });

  it('Rp18.015,00', () => {
    expect(validator('Rp18.015,00')).toBe('18.015');
  });

  it('018015', () => {
    expect(validator('018015')).toBe('018015');
  });

  it('018.015', () => {
    expect(validator('018.015')).toBe('018.015');

  });

  it('180.15', () => {
    expect(validator('180.15')).toStrictEqual(invalidSeparator);
  });

  it('18,015', () => {
    expect(validator('18,015')).toStrictEqual(invalidSeparator);
  });

  it('18,015,', () => {
    expect(validator('18,015,')).toStrictEqual(invalidSeparator);
  });

  it('18 015,', () => {
    expect(validator('18 015')).toStrictEqual(invalidSeparator);
  });

  it('18015Rp', () => {
    expect(validator('18015Rp')).toStrictEqual(wrongPosError);
  });

  it('Rp', () => {
    expect(validator('Rp')).toStrictEqual(missingError);
  });

  it('qwerty', () => {
    expect(validator('qwerty')).toStrictEqual(invalidError);
  });

});